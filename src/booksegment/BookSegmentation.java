package booksegment;

import java.util.ArrayList;
import java.util.Properties;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.LineSegmentDetector;

public class BookSegmentation {
	public Mat vi;
	Mat viLSD;
	String inFileName;
	String inFilePath;
	String inFileExt;
	Properties props=null;
	boolean empty=true;
	
	public enum orientation {olVertical, olHorizontal, olCorner};
	
	public class Line{
		
		public Point p0;
		public Point p1;
		double k;	//slope of the line in equation y=k*x+b
		double b;	// free term in equation y=k*x+b; 
		
		public Line(Point p00, Point p11){
			p0=p00;
			p1=p11;
			if((p1.x-p0.x)==0)
				k=0;
			else
				k=(p1.y- p0.y)/(p1.x - p0.x);
			b=p0.y - p0.x*k;
		}
		
		public Line(){
			p0=new Point();
			p1=new Point();
			k=0;b=0;
		}
		
		public boolean lineBelongsLine(Line l, double deltaPoint, int deltaDist){
/*			if(Math.round(p0.x)==261 && Math.round(p0.y)==56){
				Imgproc.line(debug, p0, p1, new Scalar(0, 0, 255), 3);
				Imgproc.line(debug, l.p0, l.p1, new Scalar(255, 0, 0), 3);
				Imgcodecs.imwrite("c:\\image\\debug.jpeg", debug);
			}
*/			if(k >0 && l.k<0 || k<0 && l.k>0){
				return false;
			}
			boolean retP0=(Math.abs(p0.x-l.p0.x)<=deltaPoint);
			boolean retP1=(Math.abs(p1.x-l.p1.x)<=deltaPoint);
			if(retP0 == true && retP1==true)
				return true;
			if (Math.abs(k-l.k)<6 && Math.abs(k-l.k) >=1){
				if(retP0==false || retP1==false){
					Point intersect=new Point();
					intersect.x=(l.b-b)/(k-l.k);
					intersect.y=intersect.x*k+b;
					if(Math.abs(intersect.y) < vi.rows() && intersect.y>0){
/*						Imgproc.line(debug, p0, p1, new Scalar(0, 0, 255), 3);
						Imgproc.line(debug, l.p0, l.p1, new Scalar(255, 0, 0), 3);
						Imgproc.circle(debug, intersect,3, new Scalar(150, 200, 0));
						Imgcodecs.imwrite("c:\\image\\debug.jpeg", debug);
*/						boolean retIP=(Math.abs(p1.x-intersect.x)<=deltaPoint+20);
						boolean retILP=(Math.abs(l.p0.x-intersect.x)<=deltaPoint+20);
						return retIP||retILP;
					}
					else
						return false;
				}
				return retP0&&retP1;
			}
			if((Math.abs(k-l.k)) < 1){
				double distance=Math.abs(b-l.b)/(Math.sqrt(k*k+1));
				if(distance < deltaDist)
					return true;
			}
			return false;
		}
		
		public boolean pointBelongsLine(Point p, double delta){
			boolean ret=false;
			if(Math.abs(p0.x-p.x)<=delta);
				ret=true;
			return ret;
		}
		
		public void copy(Line l){
			p0.x=l.p0.x;
			p0.y=l.p0.y;
			p1.x=l.p1.x;
			p1.y=l.p1.y;
			k=l.k;
			b=l.b;
		}
		
		public Line getNewLine(Line l){
			Line newLine=null;
			if(lineBelongsLine(l,30,30)==true){
				Point np0=new Point(), np1=new Point();
				double nk=0;
				nk=(k+ l.k)/2;
				if(p0.y < l.p0.y){
					np0.y=p0.y;
				}
				else{
					np0.y=l.p0.y;
				}
				np0.x=p0.x;
				if(p1.y > l.p1.y){
					np1.y=p1.y;
				}
				else{
					np1.y=l.p1.y;
				}
				double nb=np0.y-nk*np0.x;
				np1.x=(np1.y-nb)/nk;
				newLine=new Line(np0, np1);
/*				Imgproc.line(debug, np0, np1, new Scalar(0, 0, 255), 3);
				Imgcodecs.imwrite("c:\\image\\debug.jpeg", debug);
*/			}
		return newLine;
		}
		
		public double dist(){
			double dist=Math.sqrt((p0.x - p1.x)*(p0.x - p1.x)+(p0.y - p1.y)*(p0.y - p1.y));
			return dist;
		}
		
		public double width(){
			return Math.abs(p0.x-p1.x);
		}
		
		public double height(){
			return Math.abs(p0.y-p1.y);
		}
		
		public orientation getOrientation(){
			orientation ret=orientation.olCorner;
			if(width()*3 < height())
				ret=orientation.olVertical;
			if(height()*3 < width())
				ret=orientation.olHorizontal;
			return ret;
		}
	}
	
	ArrayList <Line> linesVertical;
	ArrayList<Line> sLinesVertical;
	ArrayList<Line> sLinesHorizontal;
	Mat linesLSD;
	
	public class Region{
		public Line lLeft;
		public Line lRight;
		public orientation orRegion;
		
		public Region(){
			lLeft=new Line();
			lRight=new Line();
			orRegion=orientation.olVertical;
		}

		public Region(Line left, Line right, orientation or){
			lLeft=new Line();
			lLeft.copy(left);
			lRight=new Line();
			lRight.copy(right);
			orRegion=or;
		}
	}
	
	public ArrayList <Object> listRegion;
	
	public BookSegmentation(Properties props, String fileName){
		linesVertical=new ArrayList<Line> ();
		listRegion=new ArrayList<Object> ();
		sLinesVertical=new ArrayList<Line>();
		sLinesHorizontal=new ArrayList<Line>();
		setProperties(props);
		vi=new Mat();
		viLSD=new Mat();
		linesLSD=new Mat();
	    viLSD = Imgcodecs.imread(fileName, Imgcodecs.IMREAD_GRAYSCALE);
	    vi = Imgcodecs.imread(fileName);
	    empty=vi.empty();
	}
	
	public boolean isEmpty(){
		return empty;
	}
	
	void setProperties(Properties props_in){
		props=props_in;
	}
	
	public void setLines(Mat lines, orientation or, int minDist){
		for(int i=0; i< lines.rows(); i++){
			Point p1=new Point();
			p1.x=lines.get(i, 0)[0];
			p1.y=lines.get(i, 0)[1];
			Point p2=new Point();
			p2.x=lines.get(i, 0)[2];
			p2.y=lines.get(i, 0)[3];
			Line l=new Line(p1, p2);
			if(l.dist() < minDist)
				continue;
			if(l.getOrientation() == or){
				if(l.p0.y>l.p1.y){
					Point p=new Point(l.p0.x, l.p0.y);
					l.p0.x=l.p1.x;
					l.p0.y=l.p1.y;
					l.p1.x=p.x;
					l.p1.y=p.y;
				}
				if(or==orientation.olVertical)
					sLinesVertical.add(l);
				if(or==orientation.olHorizontal)
					sLinesHorizontal.add(l);
			}
		}
	}
	
	public void setLinesLSD(){
		LineSegmentDetector lsd=Imgproc.createLineSegmentDetector();
		lsd.detect(viLSD, linesLSD);
/*		lsd.drawSegments(viLSD, linesLSD);
		Imgcodecs.imwrite("c:\\image\\vi1.jpeg", vi);
*/		setLines(linesLSD, orientation.olVertical, 70);
		setLines(linesLSD, orientation.olHorizontal, 100);
	}
	
	public void getLines(){
		setLinesLSD();
		detectLinesVertical();
		detectRegions();
	}
	
	public void detectRegions(){
		Line l=null, l1=null, tmp=new Line();
		for(int i=0; i < linesVertical.size();i++){
			l=linesVertical.get(i);
			for(int j=i+1; j < linesVertical.size();j++){
				l1=linesVertical.get(j);
				if(l1.p0.x < l.p0.x){
					tmp.copy(l);
					l.copy(l1);
					l1.copy(tmp);
				}
			}
		}
		double widthP0x=0;
		int count=0;
		l=null; l1=null;
		for(int i=0; i < linesVertical.size(); i++){
			l=linesVertical.get(i);
			if(l1==null){
				l1=l;
				continue;
			}
			widthP0x=widthP0x+(l.p0.x-l1.p0.x);
			count++;
			l1=l;
		}
		widthP0x=widthP0x/count;
		l=null;l1=null;
		for(int i=0; i < linesVertical.size();i++){
			l=linesVertical.get(i);
			if(l1==null){
				l1=l;
				continue;
			}
			if((l.p0.x-l1.p0.x)>widthP0x){
				Region region=new Region(l1, l, orientation.olVertical);
				listRegion.add(region);
				l1=l;
			}
			l1=l;
		}
	}
	
	public void detectLinesVertical(){
		for(int i=0; i< sLinesVertical.size(); i++){
			Line l=sLinesVertical.get(i);
			/*Line nl=*/addLine(l, orientation.olVertical);
/*			if(nl!=null){
				Imgproc.line(debug, nl.p0, nl.p1, new Scalar(0, 255, 0), 3);
				Imgcodecs.imwrite("c:\\image\\debug.jpeg", debug);
			}
*/		}
/*		for(int i=0; i < linesVertical.size(); i++){
			Line l=linesVertical.get(i);
			Imgproc.line(debug1, l.p0, l.p1, new Scalar(0, 255, 0), 3);
			Imgcodecs.imwrite("c:\\image\\debug1.jpeg", debug1);
		}
*/		for(int i=0; i < linesVertical.size(); i++){
			Line l=linesVertical.get(i);
/*			Imgproc.line(debug2, l.p0, l.p1, new Scalar(0, 0, 255), 3);
			Imgcodecs.imwrite("c:\\image\\debug2.jpeg", debug2);
*/			for(int j=0; j < linesVertical.size(); j++){
				if(i==j)
					continue;
				Line l1=linesVertical.get(j);
/*				if(Math.round(l.p0.x)==760 && Math.round(l.p0.y)==109){ 
					Imgproc.line(debug2, l1.p0, l1.p1, new Scalar(255, 0, 0), 3);
					Imgcodecs.imwrite("c:\\image\\debug2.jpeg", debug2);
				}
*/				Line ln=l.getNewLine(l1);
				if(ln!=null){
					l.copy(ln);
					linesVertical.remove(j);
					j--;
				}
			}
		}
/*		for(int i=0; i < linesVertical.size(); i++){
			Line l=linesVertical.get(i);
			Imgproc.line(debug2, l.p0, l.p1, new Scalar(0, 255, 0), 3);
			Imgcodecs.imwrite("c:\\image\\debug2.jpeg", debug2);
		}
*/		calcPointBorder();
	}
	
	public void calcPointBorder(){
		for(int i=0; i < linesVertical.size();i++){
			Line l=linesVertical.get(i);
			double distX=10000;
			int index=-1;
			for(int j=0; j < linesVertical.size(); j++){
				if(i==j)
					continue;
				Line l1=linesVertical.get(j);
				double dist=Math.abs(l.p0.x-l1.p0.x);
				if(distX > dist){
					distX=dist;
					index=j;
				}
			}
			if(index>=0){
				Line l1=linesVertical.get(index);
				l.p0.y=Math.min(l.p0.y, l1.p0.y);
				l.p0.x=(l.p0.y-l.b)/l.k;
			}
		}
		for(int i=0; i < linesVertical.size();i++){
			Line l=linesVertical.get(i);
			double distX=10000;
			int index=-1;
			for(int j=0; j < linesVertical.size(); j++){
				if(i==j)
					continue;
				Line l1=linesVertical.get(j);
				if(l1.p1.y > l.p1.y){
					double dist=Math.abs(l.p1.x-l1.p1.x);
					if(distX > dist){
						distX=dist;
						index=j;
					}
				}
			}
			if(index>=0){
				Line l1=linesVertical.get(index);
				l.p1.y=Math.max(l.p1.y, l1.p1.y);
				l.p1.x=(l.p1.y-l.b)/l.k;
			}
		}
/*		for(int i=0; i < linesVertical.size(); i++){
			Line l=linesVertical.get(i);
			Imgproc.line(debug3, l.p0, l.p1, new Scalar(0, 255, 0), 3);
			Imgcodecs.imwrite("c:\\image\\debug3.jpeg", debug3);
		}
*/	}
	
	public Line addLine(Line l, orientation orien){
		Line nl=null;
		if(linesVertical.size()==0){
			linesVertical.add(l);
			nl=l;
		}
		else{
			boolean compare=false;
			for(int i=0; i < linesVertical.size(); i++){
				Line lv=linesVertical.get(i);
				nl=lv.getNewLine(l);
				if(nl!=null){
					lv.copy(nl);
					compare=true;
					nl=null;
					break;
				}
			}
			if(compare == false){
				nl=l;
				linesVertical.add(l);
			}
		}
		return nl;
	}
}
