/* Idea of this solution based on following thesis 
 * "If books are on shelf, then at least one external contour of image is too long".

We get an external contour after the following transformations:
1. Binarization of image
2. Morphology transformation on the binary image (operation Dilate, kernel Rect(3,7))
3. Laplacian transformation on the image, which is result of step 2.  
4. Creating of list of external contours on the image, which is result of step 3.

Further, we analyze the list of the external contours and if we found the contour 
which  has a more  100 dots, we conclude, that books are on the shelf.

This solution requires the following conditions
1. Photo contains of image of one shelf with or without books
2. Photo does not contains of flares
3. Photo has a resolution 200 Dpi
 */

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.opencv.core.Core;

import utils.SaveBookRegion;
import booksegment.BookSegmentation;


public class Main {

	static BookSegmentation bs;
	static Properties props=null;
	
	static void createProperties(){
		props = new Properties();
		try{
			FileInputStream in = new FileInputStream("main.properties");
			props.load(in);
			in.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME); }

	public static void main(String[] args) {
      //  System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        createProperties();
        String inFilePath=props.getProperty("path");
        String inFileName=props.getProperty("file_name");
        String inFileExt=props.getProperty("type");
        String fileName="";
        boolean argb=false;
        for(String arg : args){
        	argb=true;
        	fileName=arg;
        	break;
        }
    	if(argb){
			bs = new BookSegmentation (props, fileName);
			if(bs.isEmpty()==false){
				bs.getLines();
				if(bs.listRegion.size() > 0){
					SaveBookRegion sbr=new SaveBookRegion(props, "books", bs.listRegion, bs.vi);
					sbr.save();
					sbr.saveImageResult();
				}
			}
    	}
    	else{
	    	for(int j=1; j<=40; j++){
				fileName=inFilePath+inFileName+j+"."+inFileExt;
				bs = new BookSegmentation (props, fileName);
				if(bs.isEmpty()==true)
					continue;
				bs.getLines();
				if(bs.listRegion.size() > 0){
					SaveBookRegion sbr=new SaveBookRegion(props, "books", bs.listRegion, bs.vi);
					sbr.save();
					sbr.saveImageResult();
				}
	    	}
    	}
	}

}
