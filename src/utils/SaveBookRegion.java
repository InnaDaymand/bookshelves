package utils;

import java.util.ArrayList;
import java.util.Properties;

import org.json.simple.JSONObject;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import booksegment.BookSegmentation.Region;

public class SaveBookRegion extends SaveObject {

	Mat imageResult;
	
	public SaveBookRegion(Properties pr, String rName, ArrayList<Object> lst, Mat image) {
		super(pr, rName,lst);
		imageResult=image;
	}

	@SuppressWarnings("unchecked")

	@Override public void saveObjectDetails(JSONObject el, Object obj){
		Region reg=(Region) obj;
		JSONObject region=new JSONObject();
		try{
			region.put("Left_X0", reg.lLeft.p0.x);
			region.put("Left_Y0", reg.lLeft.p0.y);
			region.put("Left_X1", reg.lLeft.p1.x);
			region.put("Left_Y1", reg.lLeft.p1.y);
			region.put("Right_X0", reg.lRight.p0.x);
			region.put("Right_Y0", reg.lRight.p0.y);
			region.put("Right_X1", reg.lRight.p1.x);
			region.put("Right_Y1", reg.lRight.p1.y);
			el.put("region", region);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void saveImageResult(){
		for(int i=0; i < listObject.size(); i++){
			Region r=(Region) listObject.get(i);
			Imgproc.line(imageResult, r.lLeft.p0, r.lLeft.p1, new Scalar(0, 255, 0), 3);
			Imgproc.line(imageResult, r.lRight.p0, r.lRight.p1, new Scalar(0, 255, 0), 3);
			Imgproc.line(imageResult, r.lLeft.p0, r.lRight.p0, new Scalar(0, 255, 0), 3);
			Imgproc.line(imageResult, r.lLeft.p1, r.lRight.p1, new Scalar(0, 255, 0), 3);
			Imgcodecs.imwrite(path_result+"result.jpg", imageResult);
		}
	}
}
