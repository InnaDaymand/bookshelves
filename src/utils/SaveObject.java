package utils;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Properties;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class SaveObject {
	Properties props=null;
	protected String path_result;
	String file_name;
	protected ArrayList<Object> listObject;
	String rootName;
	JSONObject rootDetail;
	JSONArray list;
	JSONObject root;
	
	public SaveObject(Properties pr, String rName, ArrayList<Object> lst){
		root=new JSONObject();
		props=pr;
		path_result=props.getProperty("path_result");
		file_name=props.getProperty("name_file_result");
		file_name=path_result+file_name;
		rootName=rName;
		listObject=lst;
	}
	
	@SuppressWarnings("unchecked")
	public void saveListObject(){
		list=new JSONArray();
		for(int i=0; i < listObject.size(); i++){
			Object obj=listObject.get(i);
			JSONObject el=saveOneObject(obj);
			list.add(el);
		}
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject saveOneObject(Object obj){
		JSONObject el=new JSONObject();
		try{
			el.put("objHash", obj.hashCode());
			saveObjectDetails(el, obj);
		}catch(Exception e){
			e.printStackTrace();
		}
		return el;
	}
	
	public void saveObjectDetails(JSONObject el, Object obj){
		
	}
	
	@SuppressWarnings("unchecked")
	public void save(){
		try{
			rootDetail = new JSONObject();
			saveListObject();
			rootDetail.put("listBooks", list);
			root.put(rootName, rootDetail);
			rootDetail.put("count", listObject.size());
	        FileWriter fw = new FileWriter(file_name);
	        fw.write(root.toJSONString());
	        fw.flush();
	        fw.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
